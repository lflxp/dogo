package middlewares

import (
	"github.com/gin-gonic/gin"
)

// func init() {
// 	dogo.NewRails().GET("/", func(c *gin.Context) {
// 		c.Redirect(302, "/admin/index")
// 		// c.Redirect(302, "/cloud/dashboard")
// 	})
// }

func RegisterIndex(c *gin.Context) {
	c.Redirect(302, "/admin/index")
}
