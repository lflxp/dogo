package middlewares

import (
	"gitee.com/lflxp/dogo"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// func init() {
// 	RegisterSwaggerMiddleware(dogo.NewRails())
// }

func RegisterSwaggerMiddleware(router *dogo.Rails) {
	// swagger
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
