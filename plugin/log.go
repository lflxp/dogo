package plugin

import (
	"sync"

	slog "github.com/go-eden/slf4go"
)

var (
	logOnce    sync.Once
	logger     slog.Logger
	IsDebugLog bool
)

func NewLogger() slog.Logger {
	logOnce.Do(func() {
		if IsDebugLog {
			slog.SetLevel(slog.DebugLevel)
		} else {
			slog.SetLevel(slog.InfoLevel)
		}

		logger = slog.GetLogger()
	})

	return logger
}
