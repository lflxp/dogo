package plugin

import (
	"sync"
	"time"

	cache "github.com/patrickmn/go-cache"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	// 有过期时间的cache
	cacheOnce  sync.Once
	localCache *cache.Cache
	// 统计缓存个数,分别是cache和redis
	cacheCount = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "Cache_Count",
			Help: "Count how many value in each cache",
		},
		[]string{
			"source",
		},
	)
)

func init() {
	prometheus.MustRegister(cacheCount)
}

// 本地缓存Cli
func NewCacheCli() *cache.Cache {
	cacheOnce.Do(func() {
		localCache = cache.New(5*time.Minute, 10*time.Minute)
	})
	return localCache
}
