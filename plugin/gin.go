package plugin

import (
	"sync"

	"github.com/gin-gonic/gin"
)

var (
	onceGin sync.Once
	engine  *gin.Engine
)

func NewGin() *gin.Engine {
	onceGin.Do(func() {
		engine = gin.Default()
	})
	return engine
}
