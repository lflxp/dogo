package plugin

import (
	"sync"

	"github.com/guonaihong/gout"
)

var (
	cli          *gout.Client
	cliSkipCheck *gout.Client
	httponce     sync.Once
)

func GoutClient(isSkipTls bool) *gout.Client {
	httponce.Do(func() {
		if isSkipTls {
			cliSkipCheck = gout.NewWithOpt(gout.WithInsecureSkipVerify())
		} else {
			cli = gout.NewWithOpt()
		}
	})

	if isSkipTls {
		return cliSkipCheck
	}
	return cli
}
