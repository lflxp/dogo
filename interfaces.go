package dogo

import (
	log "github.com/go-eden/slf4go"
	"github.com/guonaihong/gout"
	"xorm.io/xorm"

	"github.com/gin-gonic/gin"
	cache "github.com/patrickmn/go-cache"
)

type Core interface {
	Engine() *gin.Engine
	Logger() log.Logger
	HttpClient() *gout.Client
	Orm() *xorm.Engine
	Cache() *cache.Cache
}
