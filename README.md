# 介绍

本项目提炼go开发web项目中的经验和积累，组合中间件、第三方依赖，快速创建基于Go的web应用。本项目借鉴了：

* Ruby on Rails
* Django
* Gin
* HttpClient
* log4j
* xorm