package dogo

import (
	"html/template"

	"gitee.com/lflxp/dogo/plugin"
	"gitee.com/lflxp/dogo/utils"
	"gitee.com/lflxp/dogo/utils/clientgo"
	"gitee.com/lflxp/dogo/utils/proxy"
	"github.com/gin-gonic/gin"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
)

func Info(v ...interface{}) {
	NewRails().Info(v...)
}

func Infof(format string, v ...interface{}) {
	NewRails().Infof(format, v...)
}

func Debug(v ...interface{}) {
	NewRails().Debug(v...)
}

func Debugf(format string, v ...interface{}) {
	NewRails().Debugf(format, v...)
}

func Error(v ...interface{}) {
	NewRails().Error(v...)
}

func Errorf(format string, v ...interface{}) {
	NewRails().Errorf(format, v...)
}

func Warn(v ...interface{}) {
	NewRails().Warn(v...)
}

func Warnf(format string, v ...interface{}) {
	NewRails().Warnf(format, v...)
}

func Trace(v ...interface{}) {
	NewRails().Trace(v...)
}

func Tracef(format string, v ...interface{}) {
	NewRails().Tracef(format, v...)
}

func Fatal(v ...interface{}) {
	NewRails().Fatal(v...)
}

func Fatalf(format string, v ...interface{}) {
	NewRails().Fatalf(format, v...)
}

// 注册Model
func Register(data ...interface{}) error {
	return plugin.Register(data...)
}

// 获取FuncMap
func FuncMap() template.FuncMap {
	return plugin.FuncMap
}

func GetRegistered() []map[string]string {
	return plugin.GetRegistered()
}

func GetRegisterByName(name string) map[string]string {
	return plugin.GetRegisterByName(name)
}

func EditFormColumns(data map[string]string, dbinfo map[string][]byte) string {
	return plugin.EditFormColumns(data, dbinfo)
}

func Http() *utils.GoutCli {
	return NewRails().Http
}

func SendSuccessMessage(c *gin.Context, code int, data interface{}) {
	utils.SendSuccessMessage(c, code, data)
}

func SendErrorMessage(c *gin.Context, code int, errorCode string, errorMsg string) {
	utils.SendErrorMessage(c, code, errorCode, errorMsg)
}

func NewHttpProxyByGinCustom(target string, filter map[string]string) func(c *gin.Context) {
	return proxy.NewHttpProxyByGinCustom(target, filter)
}

func ClientGo() *kubernetes.Clientset {
	return clientgo.InitClient()
}

func ClientGoDynamic() dynamic.Interface {
	client, err := clientgo.InitClientDynamic()
	if err != nil {
		panic(err)
	}
	return client
}
